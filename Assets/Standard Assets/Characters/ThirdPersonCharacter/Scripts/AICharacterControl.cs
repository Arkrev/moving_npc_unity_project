using System;
using System.Collections;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;


        public enum State
        {
            PATROL,
            WORK,
            REST
        }

        public State state;
        private bool alive;

        //Variables for patrolling
        public GameObject[] waypoint;
        System.Random rnd = new System.Random();
        private int wayPointIndex = 0;
        public float patrolSpeed = 0.5f;


        //Variables for working
        public GameObject work;
        public float dailyWorkingTime;
        private float startWorkingTime = 0;

        //Variables for resting
        public GameObject home;
        public float dailyRestingTime;
        private float startRestingTime = 0;


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;
            state = AICharacterControl.State.PATROL;
            alive = true;

            //randomize the first waypoint.
            wayPointIndex = rnd.Next(0, waypoint.Length);




            StartCoroutine("FSM");
        }


        IEnumerator FSM()
        {
            while (alive)
            {
                Debug.Log("State :" + state);
                switch (state)
                {
                    case State.PATROL:
                        Patrol();
                        break;

                    case State.WORK:
                        Work();
                        break;

                    case State.REST:
                        Rest();
                        break;


                }

                yield return null;
            }
        }

        void Patrol()
        {
            agent.speed = patrolSpeed;
            if (Vector3.Distance(this.transform.position, waypoint[wayPointIndex].transform.position) >= 2)
            {

                //Debug.Log("Unit : "+this+" distance is :" + Vector3.Distance(this.transform.position, waypoint[wayPointIndex].transform.position));
                agent.SetDestination(waypoint[wayPointIndex].transform.position);
                character.Move(agent.desiredVelocity, false, false);
            }
            else if (Vector3.Distance(this.transform.position, waypoint[wayPointIndex].transform.position) < 2)
            {
                //wayPointIndex = (wayPointIndex + 1) % waypoint.Length;
                int previousWayPointIndex = wayPointIndex;
                while(wayPointIndex == previousWayPointIndex)
                {
                    wayPointIndex = rnd.Next(0, waypoint.Length);
                }
                


            }
        }

        void Work()
        {
            if( startWorkingTime == 0)
            {
                startWorkingTime = Time.time;
            }
            if ((Time.time - startWorkingTime) < dailyWorkingTime)
            {
                //WORK
                //HARDER BETTER FASTER STRONGER.
                character.Move(Vector3.zero, false, false);
                agent.isStopped = true;
               
                
            }
            else
            {
                state = AICharacterControl.State.PATROL;
                startWorkingTime = 0;
                agent.isStopped = false;
            }

        }
        void Rest()
        {
            if (startRestingTime == 0)
            {
                startRestingTime = Time.time;
            }
            if ((Time.time - startRestingTime) < dailyRestingTime)
            {
                //SLEEP
                //A LOT.
                character.Move(Vector3.zero, false, false);
                agent.isStopped = true;


            }
            else
            {
                state = AICharacterControl.State.PATROL;
                startRestingTime = 0;
                agent.isStopped = false;
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("other : " + other);
            if (other.tag == "WorkingPoint")
            {
                state = AICharacterControl.State.WORK;
            }
            else if (other.tag == "RestingPoint")
            {
                state = AICharacterControl.State.REST;
            }
        }
    }
}
