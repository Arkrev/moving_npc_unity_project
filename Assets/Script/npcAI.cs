﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class npcAI : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; }

        public enum State
        {
            PATROL,
            WORK,
            REST
        }

        public State state;
        private bool alive;

        //Variables for patrolling
        public GameObject[] waypoint;
        private int wayPointIndex = 0;
        public float patrolSpeed = 0.5f;


        //Variables for working
        public GameObject work;
        public float dailyWorkingTime;
        private float startWorkingTime;

        //Variables for resting
        public GameObject home;
        public float dailyRestingTime;
        private float startRestingTime;
        

        void Start()
        {
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;

            state = npcAI.State.PATROL;
            alive = true;

            StartCoroutine("FSM");
        }


        IEnumerator FSM()
        {
            while (alive)
            {
                
                switch (state)
                {
                    case State.PATROL:
                        Patrol();
                        Debug.Log("yolo" + Time.time);
                        break;

                    case State.WORK:
                        Work();
                        break;

                    case State.REST:
                        Rest();
                        break;

                    
                }
                
                yield return null;
            }
        }

        void Patrol()
        {
            agent.speed = patrolSpeed;
            if( Vector3.Distance (this.transform.position, waypoint[wayPointIndex].transform.position) >= 2)
            {
                
                //Debug.Log("Unit : "+this+" distance is :" + Vector3.Distance(this.transform.position, waypoint[wayPointIndex].transform.position));
                agent.SetDestination(waypoint[wayPointIndex].transform.position);
                character.Move(agent.desiredVelocity, false, false);
            }
            else if ( Vector3.Distance(this.transform.position,waypoint[wayPointIndex].transform.position) < 2)
            {
                wayPointIndex = (wayPointIndex+1)%waypoint.Length;
                

                
            }
            else
            {
                
                //If WTH => IDLE
                character.Move(Vector3.zero, false, false);
            }
        }

        void Work()
        {
            if((Time.time - startWorkingTime)< dailyWorkingTime){
                //WORK
                //HARDER BETTER FASTER STRONGER.
            }
            else
            {
                state = npcAI.State.PATROL;
            }

        }
        void Rest()
        {
            if ((Time.time - startRestingTime) < dailyRestingTime)
            {
                //SLEEP
                //A LOT.
            }
            else
            {
                state = npcAI.State.PATROL;
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.tag == "WorkingPoint")
            {
                state = npcAI.State.WORK;
            }else if(other.tag == "RestingPoint")
            {
                state = npcAI.State.REST;
            }/*else if(other.tag == "WayPoint")
            {
                wayPointIndex = (wayPointIndex + 1) % waypoint.Length;
            }*/
        }
    }
}